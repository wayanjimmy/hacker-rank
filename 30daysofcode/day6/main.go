package main

import (
	"fmt"
	"strings"
)

func leftPad(s string, padStr string, pLen int) string {
	return strings.Repeat(padStr, pLen) + s
}

func main() {
	var N int
	fmt.Scanf("%d", &N)

	for i := 1; i <= N; i++ {
		j := N - i

		fmt.Println(strings.Repeat(" ", j) + strings.Repeat("*", i))
	}
}
