package main

import "fmt"

type InputFormat struct {
	a, b, N int
}

func pow(x int, y int) int {
	result := x

	if y == 0 {
		return 1
	}

	for i := 1; i < y; i++ {
		result *= x
	}

	return result
}

func twoPowNb(n int, b int) int {
	return pow(2, n) * b
}

func main() {
	var T int

	fmt.Scanf("%d", &T)
	input := make([]InputFormat, 500)

	for i := 0; i < T; i++ {
		fmt.Scanf("%d %d %d", &input[i].a, &input[i].b, &input[i].N)
	}

	for i := 0; i < T; i++ {
		for y := 0; y < input[i].N; y++ {
			temp := twoPowNb(0, input[i].b)
			for z := 1; z <= y; z++ {
				temp += twoPowNb(z, input[i].b)
			}

			fmt.Printf("%d ", input[i].a+temp)
		}
		fmt.Println()
	}
}
