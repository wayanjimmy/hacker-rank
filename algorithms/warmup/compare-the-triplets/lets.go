package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var pointA, pointB int
	io := bufio.NewReader(os.Stdin)
	line, _ := io.ReadString('\n')
	lineB, _ := io.ReadString('\n')
	lineArr := strings.Split(line, " ")
	lineArrB := strings.Split(lineB, " ")

	for i := 0; i < len(lineArr); i++ {
		lineArr[i] = strings.TrimSpace(lineArr[i])
		lineArrB[i] = strings.TrimSpace(lineArrB[i])

		a, _ := strconv.Atoi(lineArr[i])
		b, _ := strconv.Atoi(lineArrB[i])

		if a > b {
			pointA += 1
		} else if b > a {
			pointB += 1
		}
	}

	fmt.Printf("%d %d\n", pointA, pointB)
}
