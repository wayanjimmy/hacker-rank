package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func convertLinetoSlice(value string) []int {
	split := strings.Split(value, " ")
	result := make([]int, 10)
	for i := range split {
		split[i] = strings.TrimSpace(split[i])
		a, _ := strconv.Atoi(split[i])
		result[i] = a
	}
	return result
}

func main() {
	var n, k, q int
	var inputSlice []int
	var wantToKnow [10]int
	var thatIKnow [10]int
	io := bufio.NewReader(os.Stdin)

	fmt.Scanf("%d %d %d", &n, &k, &q)
	input, _ := io.ReadString('\n')
	inputSlice = convertLinetoSlice(input)
	for i := 0; i < q; i++ {
		fmt.Scanf("%d", &wantToKnow[i])
	}

	for j := 0; j < k; j++ {
		for i := n; i >= 0; i-- {
			if i == 0 {
				inputSlice[0] = inputSlice[n]
			} else {
				inputSlice[i] = inputSlice[i-1]
			}
		}
	}

	for i := 0; i < n; i++ {
		thatIKnow[i] = inputSlice[wantToKnow[i]]
		fmt.Println(thatIKnow[i])
	}
}
