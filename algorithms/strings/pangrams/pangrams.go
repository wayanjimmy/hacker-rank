package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	io := bufio.NewReader(os.Stdin)
	line, _ := io.ReadString('\n')

	var a [26]int

	str := strings.Replace(strings.ToLower(line), " ", "", -1)

	for i := 0; i < 26; i++ {
		a[i] = 0
	}

	for _, c := range str {
		if c != 10 {
			a[c-97]++
		}
	}

	isPangram := true

	for _, i := range a {
		if i == 0 {
			isPangram = false
			break
		}
	}

	if isPangram {
		fmt.Println("pangram")
	} else {
		fmt.Println("not pangram")
	}
}
